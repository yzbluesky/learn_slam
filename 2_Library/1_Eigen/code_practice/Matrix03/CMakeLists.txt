cmake_minimum_required(VERSION 2.6)
project(matrix03)

include_directories("/usr/local/include/eigen3")

add_executable(matrix03 main.cpp)

install(TARGETS matrix03 RUNTIME DESTINATION bin)
