## orslam安装
（测试环境为虚拟机里面的ubuntu系统）

[orbSlam安装步骤参考](https://blog.csdn.net/u014709760/article/details/85253525)



由于github下载太慢，所以可以从下面链接下载Opencv：

* [安装链接1](https://blog.csdn.net/bookzhan/article/details/104753855/)
* [安装链接2](https://blog.csdn.net/appleyuchi/article/details/986210)
  										

安装过程遇到的问题以及解决办法：

* [OpenCV安装libjasper-dev依赖包错误：E: Unable to locate package libjasper-dev](https://blog.csdn.net/CAU_Ayao/article/details/83990246)
* 有时会出现解压后无makefile文件，可以尝试换一个解压包。