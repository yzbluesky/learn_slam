#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
	
	Mat dst, src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";
	src = imread("F:/opencv/test3.png");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	Mat gray_src;
	cvtColor(src,gray_src,CV_BGR2GRAY);
	imshow("gray image", gray_src);

	Mat binImg;
	adaptiveThreshold(~gray_src,binImg,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,15,-2);
	imshow("binary image", binImg);

	Mat hline = getStructuringElement(MORPH_RECT, Size(src.cols / 16 , 1), Point(-1,-1));//水平
	Mat vline = getStructuringElement(MORPH_RECT, Size(1, src.rows / 16), Point(-1, -1));//垂直
	Mat kernel = getStructuringElement(MORPH_RECT, Size(4, 4), Point(-1, -1));//矩形结构

	Mat temp;
	erode(binImg, temp, kernel);
	dilate(temp,dst, kernel);
	bitwise_not(dst,dst);
	//blur(dst, dst, Size(3, 3), Point(-1, -1));
	imshow("Final Result", dst);


	waitKey(0);
	return 0;
}

