# 双目生成点云

## 问题描述


![image-20200506162915769](figures/image-20200506162915769.png)

## 流程

### 加载数据

利用OpenCV的API即可，分别读取对应的左右相机图片和存储着双目视差数据的视差图

### 计算深度

利用:
$$
z = \frac{bf}{d}
$$
计算出左图每个像素的深度值，然后根据反投影关系，利用像素坐标和深度数据获取3D空间点

```c++
    //z=bf/d
    PointCloud cloud;
    for(int row=0; row < img_left.rows; row++)
        for(int col=0; col < img_left.cols; col++) // 遍历左图
        {
            double x,y,z;
            uchar d = img_disparity.at<uchar>(row,col);  // 深度数据，一般单位为mm，存储为无符号整形
            z = f*5.73/d; // 计算深度
            x = (col - cx) / f*z; // 反投影
            y = (row - cy) / f*z; 
            Eigen::Vector4d pc(x,y,z,img_left.at<uchar>(row,col)/255.0);
            cloud.push_back(pc);
        }
```



## 最终结果

![image-20200506162806990](figures/image-20200506162806990.png)