#include <iostream>
#include "opencv2/opencv.hpp"
#include <stdio.h>

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
    Mat M(600, 800, CV_8UC1);
    for( int i = 0; i < 5; ++i)
    {
        //获取指针时需要指定类型
        uchar * p = M.ptr<uchar>(i);
        for( int j = 0; j < 1; ++j )
        {
            double d1 = (double) ((i+j)%255);
            //用 at()读写像素时,需要指定类型
            M.at<uchar>(i,j) = d1;
            //下面代码错误,应该使用 at<uchar>()
            //但编译时不会提醒错误
            //运行结果不正确,d2 不等于 d1
            double d2 = M.at<double>(i,j);
            cout<<"using Mat_ class, d1 is "<<d1<<" d2 is "<<d2<<endl;
        }
    }
    //在变量声明时指定矩阵元素类型
    Mat_<uchar> M1 = (Mat_<uchar>&)M;
    for( int i = 0; i < 5; ++i)
    {
        //不需指定元素类型,语句简洁
        uchar * p = M1.ptr(i);
        for( int j = 0; j < 1; ++j )
        {
            double d1 = (double) ((i+j)%255);
            //直接使用 Matlab 风格的矩阵元素读写,简洁
            M1(i,j) = d1;
            double d2 = M1(i,j);
            cout<<"using Mat_ class, d1 is "<<d1<<" d2 is "<<d2<<endl;
        }
    }
    return 0;
}
