#include <iostream>
#include <opencv2/opencv.hpp>

using namespace  cv;
using namespace std;

int main()
{
    cv::Mat src, a, b,c,d;
    src=imread("../picture/item.jpeg", IMREAD_COLOR);
    b=imread("../picture/stop.png",IMREAD_COLOR);

    imshow("origin", src);
    imshow("patern", b);

    int minrow(999);
    for(int i=0; i<src.rows; ++i){
            if((int)src.ptr<uchar>(i)[1]<200)
            minrow=min(minrow, i);
    }

    Rect rect(0, 0, minrow, src.cols-1);
    a=src(rect);

    cvtColor(a, d, COLOR_BGR2BGRA);
    assert(a.data && b.data);
    cvtColor(a,c,COLOR_BGR2GRAY);
    imshow("gray", c);
    double bnum(0.0), gnum(0.0), rnum(0.0);
    cv::Mat e(b.size(), b.type());

//    for(int i=0; i<b.rows; ++i){
//        for(int j=0; j<b.cols; ++j){
//            if(b.ptr<uchar>(i)[j*3]<10 || b.ptr<uchar>(i)[j*3+1]<10 || b.ptr<uchar>(i)[j*3+2]<10 )
//            bnum=b.ptr<uchar>(i)[j*3];
//            gnum=b.ptr<uchar>(i)[j*3+1];
//            rnum=b.ptr<uchar>(i)[j*3+2];
//        }
//    }
    bnum=b.ptr<uchar>(b.rows/2)[(b.cols/2)*3];
    gnum=b.ptr<uchar>(b.rows/2)[(b.cols/2)*3+1];
    rnum=b.ptr<uchar>(b.rows/2)[(b.cols/2)*3+2];
    cout<<bnum<<" "<<gnum<<" "<<rnum<<endl;

//去水印
    int judge;
    for(int i=2; i<d.rows-2; ++i){
        for(int j=2; j<d.cols-2; ++j){

            //if(c.at<int>(i,j)>-10){   灰度图抠图
            judge=int(d.ptr<uchar>(i-2)[j*4-8]+d.ptr<uchar>(i-2)[j*4-4]+d.ptr<uchar>(i-2)[j*4]+d.ptr<uchar>(i-2)[j*4+4]+d.ptr<uchar>(i-2)[j*4+8]+
                d.ptr<uchar>(i-1)[j*4-8]+d.ptr<uchar>(i-1)[j*4-4]+d.ptr<uchar>(i-1)[j*4]+d.ptr<uchar>(i-1)[j*4+4]+d.ptr<uchar>(i-1)[j*4+8]+
                d.ptr<uchar>(i)[j*4-8]+d.ptr<uchar>(i)[j*4-4]+d.ptr<uchar>(i)[j*4]+d.ptr<uchar>(i)[j*4+4]+d.ptr<uchar>(i)[j*4+8]+
                d.ptr<uchar>(i+1)[j*4-8]+d.ptr<uchar>(i+1)[j*4-4]+d.ptr<uchar>(i+1)[j*4]+d.ptr<uchar>(i+1)[j*4+4]+d.ptr<uchar>(i+1)[j*4+8]+
                d.ptr<uchar>(i+2)[j*4-8]+d.ptr<uchar>(i+2)[j*4-4]+d.ptr<uchar>(i+2)[j*4]+d.ptr<uchar>(i+2)[j*4+4]+d.ptr<uchar>(i+2)[j*4+8]); //4x4的全1卷积核

            if(judge<5200){
                d.ptr<uchar>(i)[j*4]=bnum;
                d.ptr<uchar>(i)[j*4+1]=gnum;
                d.ptr<uchar>(i)[j*4+2]=rnum;
            }
            else{
                d.ptr<uchar>(i)[j*4]=255;
                d.ptr<uchar>(i)[j*4+1]=255;
                d.ptr<uchar>(i)[j*4+2]=255;
                d.ptr<uchar>(i)[j*4+3]=0;
            }
        }
    }


    cv::Mat dst1, dst2;
    medianBlur(d,dst1,11);  //去锯齿
    GaussianBlur(dst1, dst2, Size(5,3),1., 0.);  //光顺
    //fastNlMeansDenoisingColored(dst1,dst2,10.0, 10.0,7,21);　　//去除噪点
    imshow("unblur", dst1);

    imshow("changed" , dst2);
    imwrite("../picture/change_color.png", dst1);
    waitKey();
//     cout << "Hello World!" << endl;
    return 0;
}
